﻿using System;

namespace PrototypePattern.Application
{
    public class Student : Person
    {
        public int Curse { get; set; }

        public string GradeBook { get; set; }

        public Student()
        {

        }

        public Student(string firstName,
            string lastName,
            int age,
            int curse,
            string gradeBook)
            : base(firstName, lastName, age)
        {
            Curse = curse;

            GradeBook = gradeBook;
        }

        public override Student MyClone()
        {
            var student = new Student(FirstName,
                LastName,
                Age,
                Curse,
                GradeBook);

            return student;
        }

        public override string ToString()
        {
            return $"{ base.ToString()} {Curse} {GradeBook}";
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            var student = (Student)obj;

            return student.FirstName.Equals(FirstName)
                && student.LastName.Equals(LastName)
                && student.Age == Age
                && student.Curse == Curse
                && student.GradeBook.Equals(GradeBook);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(base.GetHashCode(), 
                FirstName,
                LastName, 
                Age,
                Curse, 
                GradeBook);
        }
    }
}
