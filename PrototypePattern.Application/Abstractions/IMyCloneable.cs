﻿namespace PrototypePattern.Application.Abstractions
{
    public interface IMyCloneable<T>
    {
        T MyClone();
    }
}
