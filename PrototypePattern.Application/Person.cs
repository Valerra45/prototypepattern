﻿using PrototypePattern.Application.Abstractions;
using System;

namespace PrototypePattern.Application
{
    public class Person : IMyCloneable<Person>, ICloneable
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int Age { get; set; }

        public Person()
        {

        }

        public Person(string firstName,
            string lastName,
            int age)
        {
            FirstName = firstName;
            LastName = lastName;
            Age = age;
        }

        public virtual Person MyClone() =>
            new Person(FirstName, LastName, Age);

        public object Clone()
        {
            return MyClone();
        }

        public override string ToString()
        {
            return $"{FirstName} {LastName} {Age}";
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            var person = (Person)obj;

            return person.FirstName.Equals(FirstName)
                && person.LastName.Equals(LastName)
                && person.Age == Age;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Age);
        }
    }
}
