﻿using System;

namespace PrototypePattern.Application
{
    public class Graduate : Student
    {
        public Person Supervisor { get; set; }

        public string Topic { get; set; }

        public Graduate()
        {

        }

        public Graduate(string firstName,
            string lastName,
            int age,
            int curse,
            string gradeBook,
            Person supervisor,
            string topic)
            : base(firstName, lastName, age, curse, gradeBook)
        {
            Supervisor = supervisor;

            Topic = topic;
        }

        public override string ToString()
        {
            return $"{ base.ToString()} {Supervisor.ToString()} {Topic}";
        }

        public override Graduate MyClone()
        {
            var graduete = new Graduate(FirstName,
                LastName,
                Age,
                Curse,
                GradeBook,
                Supervisor.MyClone(),
                Topic);

            return graduete;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            var graduete = (Graduate)obj;

            return graduete.FirstName.Equals(FirstName)
                && graduete.LastName.Equals(LastName)
                && graduete.Age == Age
                && graduete.Curse == Curse
                && graduete.GradeBook.Equals(GradeBook)
                && graduete.Supervisor.Equals(Supervisor)
                && graduete.Topic.Equals(Topic);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(base.GetHashCode(), 
                FirstName, 
                LastName,
                Age,
                Curse, 
                GradeBook, 
                Supervisor, 
                Topic);
        }
    }
}
