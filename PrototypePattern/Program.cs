﻿using PrototypePattern.Application;
using System;

namespace PrototypePattern
{
    class Program
    {
        static void Main(string[] args)
        {
            var person = new Person()
            {
                FirstName = "Ivan",
                LastName = "Ivanov",
                Age = 43
            };

            var person1 = person.MyClone();
            var person2 = (Person)person.Clone();

            Console.WriteLine($"{"Person"} {person.ToString()}");
            Console.WriteLine($"{"IMyClonable"} {person1.ToString()} {person1.Equals(person)}");
            Console.WriteLine($"{"IClonable"} {person2.ToString()} {person2.Equals(person)}");
            Console.WriteLine();

            var student = new Student()
            {
                FirstName = "Petr",
                LastName = "Petrov",
                Age = 24,
                Curse = 5,
                GradeBook = "5786906"
            };

            var student1 = student.MyClone();
            var student2 = (Student)student.Clone();

            Console.WriteLine($"{"Student"} {student.ToString()}");
            Console.WriteLine($"{"IMyClonable"} {student1.ToString()} {student1.Equals(student)}");
            Console.WriteLine($"{"IClonable"} {student2.ToString()} {student2.Equals(student)}");
            Console.WriteLine();

            var graduete = new Graduate()
            {
                FirstName = "Ivan",
                LastName = "Petrov",
                Age = 27,
                Curse = 6,
                GradeBook = "444444333",
                Supervisor = person,
                Topic = "Free electron laser"
            };

            var graduete1 = graduete.MyClone();
            var graduete2 = (Graduate)graduete.Clone();

            Console.WriteLine($"{"Graduate"} {graduete.ToString()}");
            Console.WriteLine($"{"IMyClonable"} {graduete1.ToString()} {graduete1.Equals(graduete)}");
            Console.WriteLine($"{"IClonable"} {graduete2.ToString()} {graduete2.Equals(graduete)}");
            Console.WriteLine();

            Console.ReadLine();
        }
    }
}
